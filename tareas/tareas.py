#!/usr/bin/python3
'''
Script to automatically send the home schooling emails.
 
So my kids are in homeschooling due the pandemic and I have to send *evidence* of my kid's work of their workbook via photos and email
because their teachers are stuck in 2002 I guess. And it is a PITA to do this daily, so henche a python script
 
'''
import os
import smtplib
import datetime
import mimetypes
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


def main():
    msg = MIMEMultipart()
    Kid = "Rafael"
    Group = "3-B"    
    x = datetime.datetime.now()
    Today = (x.strftime("%d-%b"))
    # setup the parameters of the message
    password = "[sender password]"
    msg['From'] = "[sender]"
    msg['To'] = "[comma separated email list] "
    msg['Subject'] = "Trabajo en casa de "+Kid+" para el dia "+Today 
    message = "Buen dia prof.\nAdjunto el trabajo en casa para "+Kid+" de "+Group+" para este dia "+Today+"\n\nSaludos!"
  
    # Attach images, this gets tricky
    directory = "/mnt/c/Users/Rafael_Rojas/Documents/homework/rafael/"
    for filename in os.listdir(directory):
        path = os.path.join(directory, filename)
        with open(path, 'rb') as img:
            image = MIMEImage(img.read())
            msg.attach(image)

    # Send the message itself 
    msg.attach(MIMEText(message, 'plain'))
    server = smtplib.SMTP('smtp.gmail.com: 587')
    server.starttls()
    server.login(msg['From'], password)
    server.sendmail(msg['From'], msg['To'], msg.as_string())
    server.quit() 



if __name__ == '__main__':
    main()

